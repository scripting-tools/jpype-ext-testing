# test that JStub works as expected

from jpype import *
from jpype_test import *
from pytest import raises


def test_mock_default_method():
    numberMock = JMock(java.lang.Number)
    assert 0.0 == numberMock.doubleValue()


# def test_mock_stubbed_with_value():
#     numberMock = JMock(java.lang.Number)
#     jStub = JStub(returns="just a number")
#     numberMock.toString = jStub
#     assert "just a number" == numberMock.toString()
#     jStub.assert_called_once()

class MyCallable():
    def __init__(self, value):
        self.value = value
        self.call_count = 0

    def my_callable(self):
        self.call_count += 1
        return self.value


def test_mock_stubbed_with_callable():
    myCallable = MyCallable(999)
    numberMock = JMock(java.lang.Number)
    numberMock.longValue = JStub(returns=myCallable.my_callable)

    assert 999 == numberMock.longValue()
    assert 1 == myCallable.call_count


def test_mock_raise_attribute_error():
    numberMock = JMock(java.lang.Number)
    with(raises(AttributeError)):
        numberMock.non_existing_method()


def test_stub_assert_called_once():
    path = JMock(java.nio.file.Path)
    file = JMock(java.io.File)
    path.toFile = JStub(returns=file)

    path.toFile()
    path.toFile.assert_called_once()
