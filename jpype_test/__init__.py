__version__ = "0.0.1"

__cmmnbuild_deps__ = [
    "mockito-core",
    "slf4j-log4j12",
    "slf4j-api",
    "log4j"
]

# When running setuptools without required dependencies installed
# we need to be able to access __version__, so print a warning but
# continue
try:
    import cmmnbuild_dep_manager as _mgr
    import jpype as _jp
    if not _jp.isJVMStarted():
        _mgr.Manager('jpype_test').start_jpype_jvm()
    from .jpype_mockito import *
    from .conversions import *
except (ImportError, TypeError) as ex:
    import logging
    logging.basicConfig()
    log = logging.getLogger(__name__)
    log.warning('required dependencies are not yet installed: {0}'.format(ex))
