import jpype
from typing import Set, List, Tuple, Mapping
from datetime import datetime

__all__ = ['to_java']

java = jpype.JPackage('java')


def to_java(value):
    if isinstance(value, Set):
        hs = java.util.HashSet()
        for v in value:
            hs.add(to_java(v))
        return hs
    if isinstance(value, List):
        hs = java.util.ArrayList()
        for v in value:
            hs.add(to_java(v))
        return hs
    if isinstance(value, Mapping):
        hs = java.util.HashMap()
        for k, v in value.items():
            hs.put(to_java(k), to_java(v))
        return hs
    if isinstance(value, datetime):
        return java.sql.Timestamp(int(value.timestamp() * 1000))
    if hasattr(value, '__javavalue__'):
        return value.__javavalue__
    return value
