from .conversions import *
import jpype
from typing import List, Tuple, Any, Callable, Optional

__all__ = ['JStub', 'JMock', 'Mockito']

java = jpype.JPackage('java')
org = jpype.JPackage('org')
Mockito = org.mockito.Mockito
ArgumentCaptor = org.mockito.ArgumentCaptor


class JStub(object):
    def __init__(self, returns: Optional[Any] = None, *, auto_convert: bool = True):
        if isinstance(returns, Callable) and auto_convert:
            self.returns = lambda *a: to_java(returns(*a))
        elif auto_convert:
            self.returns = to_java(returns)
        else:
            self.returns = returns
        self.object = None
        self.method_name = None
        self.overloads = None

    def calls(self) -> List[Tuple[Any]]:
        all_calls = []
        invocations = Mockito.mockingDetails(self.object._mock).getInvocations()
        for invocation in invocations:
            if invocation.getMethod() not in self.overloads:
                continue
            all_calls.append(tuple(invocation.getArguments()))
        return all_calls

    def assert_called(self, times: int = 1) -> List[Tuple[Any]]:
        calls = self.calls()
        assert len(calls) == times, \
            '{0}: expected {1} invocations but got {2}'.format(self.method_name, times, len(calls))
        return calls

    def assert_called_atleast(self, times: int = 1) -> List[Tuple[Any]]:
        calls = self.calls()
        assert len(calls) >= times, \
            '{0}: expected AT LEAST {1} invocations but got {2}'.format(self.method_name, times, len(calls))
        return calls

    def assert_called_once(self) -> Tuple[Any]:
        return self.assert_called(times=1)[0]

    def assert_called_atleast_once(self) -> List[Tuple[Any]]:
        return self.assert_called_atleast(times=1)

    def assert_not_called(self) -> None:
        self.assert_called(times=0)

    def __call__(self, *args, **kwargs):
        return getattr(self.object._mock, self.method_name)(*args, **kwargs)


class JMock(object):
    def __init__(self, jc):
        self._mock = Mockito.mock(jc)
        self._javaclass = jc

    def __setattr__(self, key, value):
        if isinstance(value, JStub):
            self._stub(key, value)
        super().__setattr__(key, value)

    def __getattr__(self, item):
        return getattr(self._mock, item)

    def _stub(self, method_name, mock):
        mock.object = self
        # this is to allow mocking methods "hidden" by customizers, JPype etc.
        real_method_name = method_name[1:] if method_name.startswith('_') else method_name
        mock.method_name = real_method_name
        overloads = [m for m in jpype.reflect.getMethods(self._javaclass) if m.getName() == real_method_name]
        mock.overloads = overloads
        for method in overloads:
            param_types = method.getParameterTypes()
            if method.getReturnType().name == 'void':
                if mock.returns is not None:
                    raise ValueError('"{0}" is a void method - return value must not be provided.'
                                     .format(real_method_name))
                stubber = getattr(Mockito.doNothing().when(self._mock), method_name)
            else:
                if isinstance(mock.returns, Callable):
                    mockito_cb = jpype.JProxy('org.mockito.stubbing.Answer',
                                              {'answer': lambda a: mock.returns(*(a.getArguments()))})
                    stubber = getattr(Mockito.doAnswer(mockito_cb).when(self._mock), method_name)
                else:
                    stubber = getattr(Mockito.doReturn(mock.returns, mock.returns).when(self._mock), method_name)
            stubber(*[jpype._jwrapper.JObject(Mockito.any(t), t.getName()) for t in param_types])

    def __call__(self, *args, **kwargs):
        return self._mock

    @property
    def __javavalue__(self):
        return self._mock
