#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ast
import os
import setuptools

from setuptools.command.install import install as _install


def get_version_from_init():
    init_file = os.path.join(
        os.path.dirname(__file__), 'jpype_test', '__init__.py'
    )
    with open(init_file, 'r') as fd:
        for line in fd:
            if line.startswith('__version__'):
                return ast.literal_eval(line.split('=', 1)[1].strip())


# Custom install function to install and register with cmmnbuild-dep-manager
class install(_install):
    def initialize_options(self):
        _install.initialize_options(self)

    def run(self):
        try:
            import cmmnbuild_dep_manager
            mgr = cmmnbuild_dep_manager.Manager()
            mgr.install('jpype_test')
        except ImportError:
            pass
        _install.run(self)


setuptools.setup(
    name='jpype-ext-testing',
    version=get_version_from_init(),
    description='Tools for testing JPype python-java bridges',
    author='Michi Hostettler',
    author_email='michi.hostettler@cern.ch',
    url='https://gitlab.cern.ch/scripting-tools/jpype-ext-testing',
    packages=['jpype_test'],
    package_dir={'jpype_test': 'jpype_test'},
    install_requires=['JPype1>=0.6.3',
                      'cmmnbuild-dep-manager>=2.1.2'],
    cmdclass={'install': install},
)
